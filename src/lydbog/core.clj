(ns lydbog.core
  (:use clojure.java.io)
  (:use ring.adapter.jetty)
  (:use [ring.middleware.file :only [wrap-file]])
  (:use ring.util.request)
  (:use [clojure.string :only [join blank?]])
  (:use id3)
  (:use [clojure.java.io :as io])
  (:use [hiccup.core :only [html]])
  (:gen-class))

(defn slurp-bytes
  "Slurp the bytes from a slurpable thing."
  [x]
  (with-open [out (java.io.ByteArrayOutputStream.)]
    (clojure.java.io/copy (clojure.java.io/input-stream x) out)
    (.toByteArray out)))

(defn r
  "Make a 200 response with plaintext content."
  [response]
  {:status 200
   :headers {"Content-Type" "text/plain"}
   :body response})

(defn read-id3-field
  "Return field `sym` from id3 data struct `id3-data`
Value is comma-joined string, or nil if blank."
  [id3-data sym]
  (let [fieldstr (join ", " (sym id3-data))]
    (cond
      (blank? fieldstr) nil
      :else fieldstr)))

(defn read-mp3-metadata [mp3filepath]
  "Take an mp3 file (can be anything that is accepted by `input-stream`).
Return a map with :artist and :title, or nil if it fails to parse"
  (try
    ; read id3 data and return obj
    (let [id3-data (with-mp3 [mp3 mp3filepath] (:id3/tag mp3))]
      {:author      (read-id3-field id3-data :id3.frame.name/artist)
       :description (or (read-id3-field id3-data :id3.frame.name/version) ; TIT3: Subtitle/Description refinement - No idea why id3 lib calls it "version"
                        (read-id3-field id3-data :id3.frame.name/album)
                        (read-id3-field id3-data (fn [x] (get (:id3.frame.name/custom x) "description")))
                        (read-id3-field id3-data (fn [x] (get (:id3.frame.name/custom x) "comment")))
                        (read-id3-field id3-data (fn [x] (get (:id3.frame.name/custom x) "COMM"))))
       :title       (read-id3-field id3-data :id3.frame.name/title)
       :date        (read-id3-field id3-data :id3.frame.name/original-date)})
    ; nil if reading ID3 tags fails
    (catch Exception e nil)))

(defn write-new-mp3 [path content]
  "Write `content` to a file in the dir `path` with a random new uuid
Return the generated of the file."
  (let [uuid (.toString (java.util.UUID/randomUUID))
        mp3filepath (format "%s%s.mp3" path uuid)]
    (do (with-open [w (output-stream mp3filepath)]
          (.write w content))
        (r uuid))))

(defn delete-mp3 [path uuid]
  "Delete mp3 file with uuid in dir `path`"
  (if (uuid? uuid)
    (try
      (do (delete-file (format "%s%s.mp3" path uuid)) (r ""))
      (catch java.io.IOException e {:status 410})
      (finally (r "")))
    {:status 410
     :body "Invalid Path (must be an UUID)\n"}))

(defn uri-to-uuid-maybe [uri]
  "Convert a uri string to a UUID object"
  (try
    (read-string (format "#uuid \"%s\"" (clojure.string/join (drop 1 uri))))
    (catch java.lang.IllegalArgumentException e nil)))

(defn rss-item [static-base-url item]
  "Make one RSS <item> entry for a single mp3 file"
  [:item [:title (:title (:metadata item))]
   [:description (:description (:metadata item))]
   [:author (:author (:metadata item))]
   [:googleplay:author (:author (:metadata item))]
   [:itunes:author (:author (:metadata item))]
   [:pubDate (:date (:metadata item))]
   [:guid (:filename item)]
   [:enclosure {:url (apply str [static-base-url (:filename item)])
                :type "audio/mpeg"
                :length (str (:filesize item))}]])

(defn rss-from-data [config items]
  (html [:rss {:version "2.0" :encoding "UTF-8"}
         [:channel
          (cons [:title (:podcast-name config)]
                (cons [:description "A collection of books uploaded by the public - for the public."]
                      (cons [:link "https://gitlab.com/pyjam.as/lydbog"]
                            (cons [:image [:url (:art-url config)]]
                                  (map (partial rss-item (:public-url config)) items)))))]]))

(defn renderable-file-data [file]
  {:filename (.getName file)
   :filesize (.length file)
   :metadata (read-mp3-metadata (.getPath file))})

(defn all-mp3-files [path]
  (filter (fn [f] (.endsWith (.getName f) ".mp3"))
          (file-seq (clojure.java.io/file path))))

(defn current-rss-string [config]
  "Make RSS string from the mp3 files in `path`"
  (let [all-mp3-files (filter (fn [f] (.endsWith (.getName f) ".mp3"))
                              (file-seq (file (:path config))))
        mp3-file-data (map renderable-file-data all-mp3-files)]
    (rss-from-data config mp3-file-data)))

(defn valid-mp3?
  "True if content contains an ID3 Title tag."
  [content]
  (not (blank? (:title (read-mp3-metadata content)))))

(defn handler [config]
  "Returns a function that can respond to http requests."
  (wrap-file (fn [request]
               (case (:request-method request)
                 :get {:status 200
                       :headers {"Content-Type" "application/rss+xml"}
                       :body (current-rss-string config)}
                 :post (let [content (slurp-bytes (:body request))]
                         (if (valid-mp3? content)
                           (write-new-mp3 (:path config) content)
                           {:status 422
                            :body "Invalid ID3 Metadata (Title tag required)"}))
                 :delete (delete-mp3 (:path config) (uri-to-uuid-maybe (:uri request)))
                 (r "")))
             (:path config)))

(defn create-handler []
  "Creates a handler function from environment variable config"
  (handler {:path          (or (System/getenv "LYDBOG_PATH") "/tmp/")
            :public-url    (or (System/getenv "PUBLIC_URL") "http://localhost:3000/")
            :podcast-name  (or (System/getenv "PODCAST_NAME") "Lydbog")
            :art-url       (or (System/getenv "ART_URL") "http://localhost:3000/art.png")}))

(defn -main
  [& args]
  (run-jetty (create-handler) {:port 3000}))
