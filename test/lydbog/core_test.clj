(ns lydbog.core-test
  (:require [clojure.test :refer :all]
            [lydbog.core :refer :all]))

(defn valid-mp3-content []
  (slurp-bytes "test/lydbog/impact_moderato.mp3"))

(deftest test-post
  (testing "Posting data"
    (let [r ((create-handler) {:request-method :post
                               :body (valid-mp3-content)})]
      (is (and (= (get r :status) 200))))))

(deftest id3-tags
  (testing "Read full ID3 data from test file"
    (is (= (read-mp3-metadata "test/lydbog/impact_moderato.mp3")
           {:author "Kevin MacLeod" :description "YouTube Audio Library" :title "Impact Moderato" :date nil})))
  (testing "Read ID3 data from untagged file"
    (is (= (read-mp3-metadata "test/lydbog/empty.mp3")
           nil)))
  (testing "Read ID3 data from file with only artist tag"
    (is (= (read-mp3-metadata "test/lydbog/artist-only.mp3")
           {:author "Artist" :description nil, :title nil, :date nil})))
  (testing "Read ID3 data from file with only title tag"
    (is (= (read-mp3-metadata "test/lydbog/title-only.mp3")
           {:author nil, :description nil :title "Title" :date nil}))))

(deftest test-validate-mp3
  (testing "Validate a fully-tagged mp3 (path)"
    (is (valid-mp3? "test/lydbog/impact_moderato.mp3")))
  (testing "Validate an invalid mp3 (path)"
    (is (not (valid-mp3? "test/lydbog/empty.mp3"))))
  (testing "Validate a fully-tagged mp3 (file)"
    (is (valid-mp3? (clojure.java.io/file "test/lydbog/impact_moderato.mp3")))))

(deftest test-create-followed-by-delete
  (testing "Create followed by delete"
    (let [uuid (get ((create-handler) {:request-method :post
                                       :body (valid-mp3-content)})
                    :body)
          r    ((create-handler) {:request-method :delete
                                  :uri (format "/%s" uuid)})]
      (is (= (get r :status) 200)))))

(deftest test-delete-non-uuid
  (testing "Deleting non-uuid"
    (is (= (get ((create-handler) {:request-method :delete
                                   :uri "//etc/passwd"})
                :status)
           410))))

(def default-config 
  {:public-url "http://example.org/" :podcast-name "Lydbog" :art-url "http://localhost:3000/art.png" :path "/tmp/lydbog"})

(deftest test-rss-generation
  (testing "Empty rss")
  (is (= (rss-from-data default-config [])
         "<rss encoding=\"UTF-8\" version=\"2.0\"><channel><title>Lydbog</title><description>A collection of books uploaded by the public - for the public.</description><link>https://gitlab.com/pyjam.as/lydbog</link><image><url>http://localhost:3000/art.png</url></image></channel></rss>"))
  (testing "Single-item rss"
    (is (= (rss-from-data default-config
                          [{:filename "id0.mp3"
                            :filesize 0
                            :metadata {:author "Author0" :title "Title0"}}])
           "<rss encoding=\"UTF-8\" version=\"2.0\"><channel><title>Lydbog</title><description>A collection of books uploaded by the public - for the public.</description><link>https://gitlab.com/pyjam.as/lydbog</link><image><url>http://localhost:3000/art.png</url></image><item><title>Title0</title><description></description><author>Author0</author><googleplay:author>Author0</googleplay:author><itunes:author>Author0</itunes:author><pubDate></pubDate><guid>id0.mp3</guid><enclosure length=\"0\" type=\"audio/mpeg\" url=\"http://example.org/id0.mp3\"></enclosure></item></channel></rss>")))
  (testing "Two-item rss"
    (is (= (rss-from-data default-config
                          [{:filename "id0.mp3"
                            :filesize 0
                            :metadata {:author "Author0" :title "Title0"}}
                           {:filename "id1.mp3"
                            :filesize 0
                            :metadata {:author "Author1" :title "Title1"}}])
           "<rss encoding=\"UTF-8\" version=\"2.0\"><channel><title>Lydbog</title><description>A collection of books uploaded by the public - for the public.</description><link>https://gitlab.com/pyjam.as/lydbog</link><image><url>http://localhost:3000/art.png</url></image><item><title>Title0</title><description></description><author>Author0</author><googleplay:author>Author0</googleplay:author><itunes:author>Author0</itunes:author><pubDate></pubDate><guid>id0.mp3</guid><enclosure length=\"0\" type=\"audio/mpeg\" url=\"http://example.org/id0.mp3\"></enclosure></item><item><title>Title1</title><description></description><author>Author1</author><googleplay:author>Author1</googleplay:author><itunes:author>Author1</itunes:author><pubDate></pubDate><guid>id1.mp3</guid><enclosure length=\"0\" type=\"audio/mpeg\" url=\"http://example.org/id1.mp3\"></enclosure></item></channel></rss>"))))

(deftest test-post-then-get
  (testing "Upload mp3, then download it"
    (let [content (valid-mp3-content)
          uuid    (get ((create-handler) {:request-method :post :body content})
                       :body)
          uri     (format "/%s.mp3" uuid)]
      (is (java.util.Arrays/equals (slurp-bytes (get ((create-handler) {:request-method :get
                                                                        :uri uri})
                                                     :body))
                                   content)))))

(deftest upload-file-then-read-rss
  (testing "Upload file, and see that it appears in the rss feed"
    (let [tempdir-name   (format "/tmp/%s/" (.toString (java.util.UUID/randomUUID)))
          tmpdir-success (.mkdir (clojure.java.io/file tempdir-name))
          test-handler   (handler {:path tempdir-name
                                   :public-url "http://example.org/"
                                   :art-url "http://localhost:3000/art.png"
                                   :podcast-name "Lydbog"})
          uuid           (get (test-handler {:request-method :post
                                             :body (slurp-bytes "test/lydbog/impact_moderato.mp3")})
                              :body)
          rss            (get (test-handler {:request-method :get
                                             :uri "/"})
                              :body)]
      (is (= rss
             (format "<rss encoding=\"UTF-8\" version=\"2.0\"><channel><title>Lydbog</title><description>A collection of books uploaded by the public - for the public.</description><link>https://gitlab.com/pyjam.as/lydbog</link><image><url>http://localhost:3000/art.png</url></image><item><title>Impact Moderato</title><description>YouTube Audio Library</description><author>Kevin MacLeod</author><googleplay:author>Kevin MacLeod</googleplay:author><itunes:author>Kevin MacLeod</itunes:author><pubDate></pubDate><guid>%s.mp3</guid><enclosure length=\"764176\" type=\"audio/mpeg\" url=\"http://example.org/%s.mp3\"></enclosure></item></channel></rss>"
                     uuid uuid))))))
