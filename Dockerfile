FROM clojure:openjdk-11-lein-2.9.6

WORKDIR /app

COPY ./project.clj .
RUN lein deps

COPY ./test /app/test
COPY ./src /app/src

RUN lein uberjar

EXPOSE 3000

CMD ["java", "-jar", "./target/uberjar/lydbog-0.1.0-standalone.jar"]
