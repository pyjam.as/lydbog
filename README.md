# lydbog

## Introduction

We made this because we were missing a good way to self-host audiobooks.

Turns out that podcast apps are really good for playing audiobooks too, and since podcasts are all RSS, it's very easy to self-host.

So we made this project: just a tiny service that you can upload MP3 files to, which are then added to a podcast-compatible RSS feed with their respective metadata.

This README refers to the audiobook usecase a lot, but I guess it could be used for anything MP3 if you want.


## Usage

### Usage: Listening from a Podcast App

Most podcast apps have an "Add by RSS address" button.

Find that button. Click it. Then type the URL of your lydbog instance.

### Usage: Adding MP3 Files

#### Step 1: Prepare the MP3 File

The MP3 file should be one single file for each audiobook.
If your audiobook is split into chapters, you want to concatenate these into a single MP3 file first.

This can be done in many ways. Some people can do it with ffmpeg. I like pydub.

```
TODO: add a cli snippet for concatting audiobooks
```

Your long MP3 file needs to have metadata.
The bare minimum is setting the *Artist* and *Title* fields, but other fields are supported too. A complete list:
| id3v2.4.0 Field      | Podcast RSS Field | Description                                                                                                                            |
| -------------------- | ----------------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| title / TIT2         | title             | The title of the book                                                                                                                  |
| artist / TPE1        | author            | The author of the book                                                                                                                 |
| subtitle / TIT3      | description       | Description/teaser for the book                                                                                                        |
| original-date / TDOR | pubDate           | The date the book was released<br>Try to keep it in the format:<br>`Mon, 21 Feb 1848 00:00:00 UTC`<br> as most clients understand that |

You can use any tool for setting this data, but keep in mind that they have to support id3v2.**4**.0 (some only support 2.**3**.0).
`ffmpeg` is pretty good for this: 

```
$ ffmpeg -i 1984_audiobook.mp3 -metadata title="1984" \
                               -metadata artist="George Orwell" \
                               -metadata TDOR="Wed, 8 Jun 1949 00:00:00 UTC" \
                               -metadata TIT3="George Orwell depicts a gray, totalitarian world..." \
                               -c:a copy 1984_audiobook_metadata.mp3
```

Please use `&lt;br&gt;` for line-breaks in your description/comment.

#### Step 2: Upload it

You just post the thing to the service.

Using httpie:

```
$ http post 'https://lydbog.example.com/' < ./1984_audiobook.mp3
```
or curl:
```
$ curl -X POST --data-binary @1984_audiobook.mp3 https://lydbog.example.com/
```

### Usage: Self-hosting

We distribute a Docker image, available from the gitlab registry at this URL:

```registry.gitlab.com/pyjam.as/lydbog:main```

It can be configured using environment variables:

- `LYDBOG_PATH`: the path to store and read MP3 files from (default: `/tmp`).
- `PUBLIC_URL`: the public URL of this lydbog instance (default: `http://localhost:3000`).
- `PODCAST_NAME`: the name of the "podcast" (default: `Lydbog`).
- `ART_URL`: URL of an image to use as cover art for the podcast (default: `http://localhost:3000/art.png`).

If you don't know how to use docker, you can probably ask the internet or something.
