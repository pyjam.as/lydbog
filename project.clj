(defproject lydbog "0.1.0"
  :description "A HTTP Service that lets users upload audio files, and download them via rss"
  :url "http://pyjam.as"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [ring/ring-core "1.9.3"]
                 [ring/ring-jetty-adapter "1.9.3"]
                 [zsau/id3 "1.0.0"]
                 [hiccup "1.0.5"]]
  :plugins [[lein-cljfmt "0.8.0"]]
  :main ^:skip-aot lydbog.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
